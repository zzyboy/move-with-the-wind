function success(msg, data) {
    if (!data) {
        data = ''
    }
    if (!msg) {
        msg = ''
    }
    return {
        code: 200,
        data,
        msg
    }
}

//返回失败信息 服务异常
function fail(msg, data) {
    if (!data) {
        data = ''
    }
    if (!msg) {
        msg = ''
    }
    return {
        code: 500,
        data,
        msg
    }
}

function failAuth(msg, data) {
    if (!data) {
        data = ''
    }
    if (!msg) {
        msg = ''
    }
    return {
        code: 400,
        data,
        msg
    }
}

module.exports = {
    success,
    fail,
    failAuth
}

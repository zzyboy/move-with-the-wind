
const {
    exec
} = require('../db/mysql')

var moment = require('moment');

//查询所有搬家信息
const moveList = () => {
    const sql = `select * from move_info`
    return exec(sql).then(rows => {
        return rows || {}
    })
}

//增加搬家信息
const addMoveInfo = (data) => {
    let myDate = moment(data.move_date).format("YYYY-MM-DD HH:mm:ss");
    const sql = `insert into move_info(area,car_type,move_date,contact,phone,username) values('${data.area}','${data.car_type}','${myDate}','${data.contact}','${data.phone}','${data.username}')`
    return exec(sql).then(rows => {
        return rows || {}
    })
}

//修改搬家信息
const updateMoveInfo = (data) => {
    const sql = `update move_info set status='${data.status}' where move_id='${data.move_id}'`
    return exec(sql).then(rows => {
        return rows || {}
    })
}

//删除搬家信息
const deleteMoveInfo = (data) => {
    const sql = `delete from move_info where move_id='${data.move_id}'`
    return exec(sql).then(rows => {
        return rows || {}
    })
}

//根据id查询搬家信息
const selectById = (data) => {
    const sql = `select * from move_info where move_id=${data}`
    return exec(sql).then(rows => {
        return rows || {}
    })
}

module.exports = {
    moveList,
    addMoveInfo,
    updateMoveInfo,
    deleteMoveInfo,
    selectById
}

//引入处理post请求参数的组件
const bodyParser = require('body-parser');

//引入服务js
const app = require('../bin/www')

const user = require('./router/user')

const moveInfo = require('./router/moveInfo')
//跨域解决方案
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Content-Type", "application/json;charset=utf-8");
    res.header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    if (req.method.toLowerCase() == 'options')
        res.send(200); //让options尝试请求快速结束
    else {
        console.log('有请求进入'+req.path)
        if (req.path === '/user/login') {
            next();
        } else {
            next();
        }
        // 直接放行
    }
});

//
// app.all('*', function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Content-Type", "application/json;charset=utf-8");
//     res.header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
//     res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
//     res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
//     if (req.method.toLowerCase() == 'options')
//         res.send(200); //让options尝试请求快速结束
//
//     console.log(req)
// })




//使用bodyParser对post请求参数解析成json
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())

app.use('/user', user)
app.use('/moveInfo', moveInfo)


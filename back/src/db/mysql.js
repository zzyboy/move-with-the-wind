const mysql = require('mysql')


const { MYSQL_CONF } = require('../config/mysqlConfig')

let conn = null

//连接数据库
function handleDisconnection() {
    let connection = mysql.createConnection(MYSQL_CONF);
    //连接
    connection.connect(function (err) {
        if (err) {
            console.log(err)
            //重新连接
            setTimeout(handleDisconnection(), 2000);
        }
    });
    //执行报错拦截
    connection.on('error', function (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.log('重连')
            handleDisconnection();
        } else {
            throw err;
        }
    });
    conn = connection
}

// 统一执行sql的函数 暴露接口
function exec(sql) {
    //连接数据库
    //每一次执行sql语句 重新连接数据库
    handleDisconnection()
    const promise = new Promise((resolve, reject) => {
        conn.query(sql, (err, result) => {
            if (err) return reject(err)
            return resolve(result)
        })
        conn.end()
    })
    return promise
}
module.exports = {
    exec
}

// 引入express模块
const express = require('express')
// 创建 user路由
const user = express.Router()
// 引入body-parser
const bodyParser = require('body-parser')
// 创建 application/x-www-form-urlencoded 编码解析
const urlEncodeParser = bodyParser.urlencoded({ extended: false })

//从../controller/user 引入sql方法
const {
    userList,
    addUser,
    selectById,
    deleteUser,
    updateUser,
    selectByUsername,
    updateAddressById
} = require('../controller/user')

//引入成功失败 返回方法
const {
    success,
    fail, failAuth
} = require('../model/ajaxResult')


//写接口   /user/userList
user.get('/userList', async (req, res) => {
    //sql返回值
    const result = await userList()
    //返回给前端
    res.send(success('查询成功', result))
})

// 通过id查询
user.get('/selectById', urlEncodeParser, async (req, res) => {
    const user_id = req.query.user_id
    const result = await selectById(user_id)
    res.send(success('查询成功', result))
})

// 通过username查询
user.get('/selectByUsername', urlEncodeParser, async (req, res) => {
    const username = req.query.username
    const result = await selectByUsername(username)
    res.send(success('查询成功', result))
})

user.post('/login', urlEncodeParser,async  (req,res) => {
    const username = req.body.username
    const password = req.body.password
    if (req.body.username === '' || req.body.username === null) {
        res.send(fail('用户名不能为空'))
    }
    if (req.body.password === '' || req.body.password === null) {
        res.send(fail('用户密码不为空'))
    }
    const user = await selectByUsername(username)
    if (user===[] ||user.password !== password) {
        res.send(failAuth("密码或用户名错误"))
    }
    res.send(success("登录成功",user))

})


// 增加用户
user.post('/addUser', urlEncodeParser, async (req,res) => {
    if (req.body.username === '' || req.body.username === null) {
        res.send(fail('用户名不能为空'))
        return
    }
    if (req.body.password === '' || req.body.password === null) {
        res.send(fail('用户密码不为空'))
        return
    }
    if (req.body.phone === '' || req.body.phone === null) {
       res.send(fail('手机号不为空'))
        return
    }
    if (req.body.address === '' || req.body.address === null) {
        res.send(fail('地址不为空'))
        return
    }
    const users = await userList()
    users.forEach(u=>{
        if (u.username === req.body.username) {
            res.send(fail('已有相同用户名'))
        }
    })
    const result = await addUser(req.body)
    res.send(success('添加成功', result))
})

// 删除用户
user.delete('/deleteUser', urlEncodeParser, async (req, res) => {
    const user_id = req.query.user_id
    const result = await deleteUser(user_id)
    res.send(success('删除成功',result))
})

// 修改用户
user.put('/updateUser', urlEncodeParser, async (req, res) => {
    if (req.body.username.length < 3 || req.body.username.length > 8) {
        res.send(fail('用户名的长度在3到8之间'))
        return
    }
    if (req.body.password.length < 3 || req.body.password.length > 10) {
        res.send(fail('密码的长度在3-10之间'))
        return
    }
    if (req.body.phone !== 11) {
        res.send(fail('手机号长度为11'))
        return
    }
    const result = await updateUser(req.body)
    res.send(success('修改成功', result))
})

// 根据id修改地址
user.put('/updateAddressById', urlEncodeParser, async (req, res) => {
    const result = await updateAddressById(req.body)
    res.send(success('修改地址成功', result))
})

// 暴露
module.exports = user

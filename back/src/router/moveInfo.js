var moment = require('moment')

const express = require('express')

const moveInfo = express.Router()

const bodyParser = require('body-parser')

const urlEncodeParser = bodyParser.urlencoded({ extended: false })

const {
    moveList,
    addMoveInfo,
    updateMoveInfo,
    deleteMoveInfo,
    selectById
} = require('../controller/moveInfo')

const {
    success,
    fail
} = require('../model/ajaxResult')

//查询所有搬家信息
moveInfo.get('/moveList',async (req,res) => {
    const result = await moveList()
    for (let i = 0;i < result.length;i++) {
        result[i]['move_date'] = moment(result[i]['move_date']).format("YYYY-MM-DD HH:mm:ss");
    }
    res.send(success('查询成功',result))
})

//根据id查询搬家信息
moveInfo.get('/selectById',urlEncodeParser,async (req,res) => {
    const move_id = req.query.move_id
    const result = await selectById(move_id)
    result[0].move_date = moment(result.move_date).format("YYYY-MM-DD HH:mm:ss");
    res.send(success('查询成功',result))
})

//新增搬家信息
moveInfo.post('/addMoveInfo',urlEncodeParser,async (req,res) => {
    if (req.body.area === '' || req.body.area === null) {
        res.send(fail('地区不能为空'))
        return
    }
    if (req.body.carType === '' || req.body.carType === null) {
        res.send(fail('车辆类型不能为空'))
        return
    }
    if (req.body.movedate === null) {
        res.send(fail('搬家时间不能为空'))
        return
    }
    if (req.body.contact === '' || req.body.contact === null) {
        res.send(fail('联系人不能为空'))
        return
    }
    if (req.body.phone === '' || req.body.phone === null) {
        res.send(fail('电话不能为空'))
        return
    }
    if (req.body.username === '' || req.body.username === null) {
        res.send(fail('用户名不能为空'))
        return
    }
    const result = await addMoveInfo(req.body)
    res.send(success('添加成功',result))
})

//修改搬家信息
moveInfo.put('/updateMoveInfo',urlEncodeParser,async (req,res) => {
    const result = await updateMoveInfo(req.body)
    res.send(success('修改成功',result))
})

//删除搬家信息
moveInfo.delete('/deleteMoveInfo',urlEncodeParser,async (req,res) => {
    const result = await deleteMoveInfo(req.body)
    res.send(success('删除成功',result))
})

module.exports = moveInfo

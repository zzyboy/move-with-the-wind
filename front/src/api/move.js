import request from "@/tool/request";

export function getMoveInfo(){
    return request({
        url: '/moveInfo/moveList',
        method:'get'
    })
}

export function modifyMoveInfo(data) {
    return request({
        url: '/moveInfo/updateMoveInfo',
        method: 'put',
        data
    })
}

export function findById(data) {
    return request({
        url: `/moveInfo/selectById?move_id=${data}`,
        method: 'get',
        data
    })
}

export function addMove(data){
    return request({
        url: '/moveInfo/addMoveInfo',
        method: 'post',
        data
    })
}

export function delMoveInfo(data){
    return request({
        url: `/moveInfo/deleteMoveInfo?`,
        method: 'delete',
        data
    })
}

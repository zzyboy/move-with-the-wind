import request from "@/tool/request";

export function getUserById(data) {
    return request({
        url: `/user/selectById?user_id=${data}`,
        method: 'get',
        data
    })
}

export function getAllUser() {
    return request({
        url: '/user/userList',
        method:'get'
    })
}


export function updateUser(data) {
    return request({
        url: '/user/updateUser',
        method: 'put',
        data
    })
}

export function addUser(data) {
    return request({
        url: '/user/addUser',
        method: 'post',
        data
    })
}

export function deleteUser(data) {
    return request({
        url: `/user/deleteUser?user_id=${data}`,
        method: 'delete',
        data
    })
}

export function getByUsername(data) {
    return request({
        url: `/user/selectByUsername?username=${data}`,
        method: 'get',
        data
    })
}

export function login(data) {
    return request({
        url: '/user/login',
        method:'post',
        data
    })
}

export function updateAddressById(data) {
    return request({
        url: '/user/updateAddressById',
        method: 'put',
        data
    })
}
